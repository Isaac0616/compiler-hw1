#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <string.h>
#include "header.h"

int main( int argc, char *argv[] )
{
    FILE *source, *target;
    Program program;
    SymbolTable symtab;

    if( argc == 3){
        source = fopen(argv[1], "r");
        target = fopen(argv[2], "w");
        if( !source ){
            printf("can't open the source file\n");
            exit(2);
        }
        else if( !target ){
            printf("can't open the target file\n");
            exit(2);
        }
        else{
            program = parser(source);
            fclose(source);
            symtab = build(program);
            check(&program, &symtab);
            gencode(program, target, &symtab); /* Add &symtab as parameter */
        }
    }
    else{
        printf("Usage: %s source_file target_file\n", argv[0]);
    }

    return 0;
}


/********************************************* 
  Scanning 
 *********************************************/
Token getNumericToken( FILE *source, char c )
{
    Token token;
    int i = 0;

    while( isdigit(c) ) {
        token.tok[i++] = c;
        c = fgetc(source);
    }

    if( c != '.' ){
        ungetc(c, source);
        token.tok[i] = '\0';
        token.type = IntValue;
        return token;
    }

    token.tok[i++] = '.';

    c = fgetc(source);
    if( !isdigit(c) ){
        ungetc(c, source);
        printf("Expect a digit : %c\n", c);
        exit(1);
    }

    while( isdigit(c) ){
        token.tok[i++] = c;
        c = fgetc(source);
    }

    ungetc(c, source);
    token.tok[i] = '\0';
    token.type = FloatValue;
    return token;
}

Token scanner( FILE *source )
{
    char c;
    Token token;

    while( !feof(source) ){
        c = fgetc(source);

        while( isspace(c) ) c = fgetc(source);

        if( isdigit(c) )
            return getNumericToken(source, c);

        /************************ My Code ************************/
        /**
         * Change the code to get identifier with variable length.
         */
        token.tok[0] = c;
        if( islower(c) ){
            c = fgetc(source);
            int i = 1;
            while( islower(c) ){
                token.tok[i++] = c;
                c = fgetc(source);
            }
            token.tok[i] = '\0';
            ungetc(c, source);

            if( token.tok[0] == 'f' && token.tok[1] == '\0')
                token.type = FloatDeclaration;
            else if( token.tok[0] == 'i' && token.tok[1] == '\0')
                token.type = IntegerDeclaration;
            else if( token.tok[0] == 'p' && token.tok[1] == '\0')
                token.type = PrintOp;
            else
                token.type = Alphabet;

            return token;
        }
        /************************ My Code ************************/

        token.tok[1] = '\0';
        switch(c){
            case '=':
                token.type = AssignmentOp;
                return token;
            case '+':
                token.type = PlusOp;
                return token;
            case '-':
                token.type = MinusOp;
                return token;
            case '*':
                token.type = MulOp;
                return token;
            case '/':
                token.type = DivOp;
                return token;
            case EOF:
                token.type = EOFsymbol;
                token.tok[0] = '\0';
                return token;
            default:
                printf("Invalid character : %c\n", c);
                exit(1);
        }
    }

    token.tok[0] = '\0';
    token.type = EOFsymbol;
    return token;
}


/********************************************************
  Parsing
 *********************************************************/
Declaration parseDeclaration( FILE *source, Token token )
{
    Token token2;
    switch(token.type){
        case FloatDeclaration:
        case IntegerDeclaration:
            token2 = scanner(source);
            if (strcmp(token2.tok, "f") == 0 ||
                    strcmp(token2.tok, "i") == 0 ||
                    strcmp(token2.tok, "p") == 0) {
                printf("Syntax Error: %s cannot be used as id\n", token2.tok);
                exit(1);
            }
            return makeDeclarationNode( token, token2 );
        default:
            printf("Syntax Error: Expect Declaration %s\n", token.tok);
            exit(1);
    }
}

Declarations *parseDeclarations( FILE *source )
{
    Token token = scanner(source);
    Declaration decl;
    Declarations *decls;
    int i; /** counter for unget a string. */
    switch(token.type){
        case FloatDeclaration:
        case IntegerDeclaration:
            decl = parseDeclaration(source, token);
            decls = parseDeclarations(source);
            return makeDeclarationTree( decl, decls );
        case PrintOp:
        case Alphabet:
            for(i = strlen(token.tok)-1;i >= 0;i--)     /** Need to unget a string. */
                ungetc(token.tok[i], source);           /*                          */
            return NULL;
        case EOFsymbol:
            return NULL;
        default:
            printf("Syntax Error: Expect declarations %s\n", token.tok);
            exit(1);
    }
}

Expression *parseValue( FILE *source )
{
    Token token = scanner(source);
    Expression *value = (Expression *)malloc( sizeof(Expression) );
    value->leftOperand = value->rightOperand = NULL;

    switch(token.type){
        case Alphabet:
            (value->v).type = Identifier;
            (value->v).val.id = (char *)malloc(strlen(token.tok)+1);  /** Assign string rather than char. */
            strcpy( (value->v).val.id, token.tok);                    /*                                  */
            break;
        case IntValue:
            (value->v).type = IntConst;
            (value->v).val.ivalue = atoi(token.tok);
            break;
        case FloatValue:
            (value->v).type = FloatConst;
            (value->v).val.fvalue = atof(token.tok);
            break;
        default:
            printf("Syntax Error: Expect Identifier or a Number %s\n", token.tok);
            exit(1);
    }

    return value;
}

Expression *parseExpressionTail( FILE *source, Expression *lvalue )
{
    Token token = scanner(source);
    Expression *expr;
    int i; /** counter for unget a string. */

    switch(token.type){
        case PlusOp:
            expr = (Expression *)malloc( sizeof(Expression) );
            (expr->v).type = PlusNode;
            (expr->v).val.op = Plus;
            expr->leftOperand = lvalue;
            expr->rightOperand = parseValue(source);
            return parseExpressionTail(source, expr);
        case MinusOp:
            expr = (Expression *)malloc( sizeof(Expression) );
            (expr->v).type = MinusNode;
            (expr->v).val.op = Minus;
            expr->leftOperand = lvalue;
            expr->rightOperand = parseValue(source);
            return parseExpressionTail(source, expr);
        /************************ My Code ************************/
        /**
         * Add the case of multiply and divde.
         * It does the same thing like plus and minus,
         * but modified to concern about priority.
         * If previous operator is plus or minus,
         * it takes the right child of previous expression's root as it's left child,
         * and make's the right child of previous expression's root itslef.
         * For example, 1+2*3  ->   +  * 3 ->   +
         *                         / \         / \ 
         *                        1   2       1   *
         *                                       / \
         *                                      2   3
         */
        case MulOp:
            expr = (Expression *)malloc( sizeof(Expression) );
            (expr->v).type = MulNode;
            (expr->v).val.op = Mul;
            if(lvalue->v.type == MulNode || lvalue->v.type == DivNode){
                expr->leftOperand = lvalue;
                expr->rightOperand = parseValue(source);
                return parseExpressionTail(source, expr);
            }
            else{
                expr->leftOperand = lvalue->rightOperand;
                lvalue->rightOperand = expr;
                expr->rightOperand = parseValue(source);
                return parseExpressionTail(source, lvalue);
            }
        case DivOp:
            expr = (Expression *)malloc( sizeof(Expression) );
            (expr->v).type = DivNode;
            (expr->v).val.op = Div;
            if(lvalue->v.type == MulNode || lvalue->v.type == DivNode){
                expr->leftOperand = lvalue;
                expr->rightOperand = parseValue(source);
                return parseExpressionTail(source, expr);
            }
            else{
                expr->leftOperand = lvalue->rightOperand;
                lvalue->rightOperand = expr;
                expr->rightOperand = parseValue(source);
                return parseExpressionTail(source, lvalue);
            }
        /************************ My Code ************************/
        case Alphabet:
        case PrintOp:
            for(i = strlen(token.tok)-1;i >= 0;i--)     /** Need to unget a string. */
                ungetc(token.tok[i], source);           /*                          */
            return lvalue;
        case EOFsymbol:
            return lvalue;
        default:
            printf("Syntax Error: Expect a numeric value or an identifier %s\n", token.tok);
            exit(1);
    }
}

Expression *parseExpression( FILE *source, Expression *lvalue )
{
    Token token = scanner(source);
    Expression *expr;
    int i; /** counter for unget a string. */

    switch(token.type){
        case PlusOp:
            expr = (Expression *)malloc( sizeof(Expression) );
            (expr->v).type = PlusNode;
            (expr->v).val.op = Plus;
            expr->leftOperand = lvalue;
            expr->rightOperand = parseValue(source);
            return parseExpressionTail(source, expr);
        case MinusOp:
            expr = (Expression *)malloc( sizeof(Expression) );
            (expr->v).type = MinusNode;
            (expr->v).val.op = Minus;
            expr->leftOperand = lvalue;
            expr->rightOperand = parseValue(source);
            return parseExpressionTail(source, expr);
        /************************ My Code ************************/
        /**
         * Add the case of multiply and divde.
         * It does the same thing like plus and minus.
         * Make a node with two child which are it's operand.
         * For example, 1*2 ->   *
         *                      / \
         *                     1   2
         */
        case MulOp:
            expr = (Expression *)malloc( sizeof(Expression) );
            (expr->v).type = MulNode;
            (expr->v).val.op = Mul;
            expr->leftOperand = lvalue;
            expr->rightOperand = parseValue(source);
            return parseExpressionTail(source, expr);
        case DivOp:
            expr = (Expression *)malloc( sizeof(Expression) );
            (expr->v).type = DivNode;
            (expr->v).val.op = Div;
            expr->leftOperand = lvalue;
            expr->rightOperand = parseValue(source);
            return parseExpressionTail(source, expr);
        /************************ My Code ************************/
        case Alphabet:
        case PrintOp:
            for(i = strlen(token.tok)-1;i >= 0;i--)     /** Need to unget a string. */
                ungetc(token.tok[i], source);           /*                          */
            return NULL;
        case EOFsymbol:
            return NULL;
        default:
            printf("Syntax Error: Expect a numeric value or an identifier %s\n", token.tok);
            exit(1);
    }
}

Statement parseStatement( FILE *source, Token token )
{
    Token next_token;
    Expression *value, *expr;

    switch(token.type){
        case Alphabet:
            next_token = scanner(source);
            if(next_token.type == AssignmentOp){
                value = parseValue(source);
                expr = parseExpression(source, value);
                return makeAssignmentNode(token.tok, value, expr); /** Pass first parameter as string pointer rather than a char. */
            }
            else{
                printf("Syntax Error: Expect an assignment op %s\n", next_token.tok);
                exit(1);
            }
        case PrintOp:
            next_token = scanner(source);
            if(next_token.type == Alphabet)
                return makePrintNode(next_token.tok); /** Pass string rather than char. */
            else{
                printf("Syntax Error: Expect an identifier %s\n", next_token.tok);
                exit(1);
            }
            break;
        default:
            printf("Syntax Error: Expect a statement %s\n", token.tok);
            exit(1);
    }
}

Statements *parseStatements( FILE * source )
{

    Token token = scanner(source);
    Statement stmt;
    Statements *stmts;

    switch(token.type){
        case Alphabet:
        case PrintOp:
            stmt = parseStatement(source, token);
            stmts = parseStatements(source);
            return makeStatementTree(stmt , stmts);
        case EOFsymbol:
            return NULL;
        default:
            printf("Syntax Error: Expect statements %s\n", token.tok);
            exit(1);
    }
}


/*********************************************************************
  Build AST
 **********************************************************************/
Declaration makeDeclarationNode( Token declare_type, Token identifier )
{
    Declaration tree_node;

    switch(declare_type.type){
        case FloatDeclaration:
            tree_node.type = Float;
            break;
        case IntegerDeclaration:
            tree_node.type = Int;
            break;
        default:
            break;
    }
    tree_node.name = (char *)malloc( strlen(identifier.tok) + 1 ); /** Copy more than one character. */
    strcpy(tree_node.name, identifier.tok);                        /*                                */

    return tree_node;
}

Declarations *makeDeclarationTree( Declaration decl, Declarations *decls )
{
    Declarations *new_tree = (Declarations *)malloc( sizeof(Declarations) );
    new_tree->first = decl;
    new_tree->rest = decls;

    return new_tree;
}


Statement makeAssignmentNode( char *id, Expression *v, Expression *expr_tail ) /** Receive first parameter as string pointer rather than a char. */
{
    Statement stmt;
    AssignmentStatement assign;

    stmt.type = Assignment;
    assign.id = (char *)malloc( strlen(id) + 1 ); /** Copy String rather than character. */
    strcpy(assign.id, id);                        /*                                     */
    if(expr_tail == NULL)
        assign.expr = v;
    else
        assign.expr = expr_tail;
    stmt.stmt.assign = assign;

    return stmt;
}

Statement makePrintNode( char *id ) /* Get string rather than char. */
{
    Statement stmt;
    stmt.type = Print;
    stmt.stmt.variable = (char *)malloc( strlen(id) + 1 ); /* Malloc and copy string. */
    strcpy(stmt.stmt.variable, id);                       /*                         */

    return stmt;
}

Statements *makeStatementTree( Statement stmt, Statements *stmts )
{
    Statements *new_tree = (Statements *)malloc( sizeof(Statements) );
    new_tree->first = stmt;
    new_tree->rest = stmts;

    return new_tree;
}

/* parser */
Program parser( FILE *source )
{
    Program program;

    program.declarations = parseDeclarations(source);
    program.statements = parseStatements(source);

    return program;
}


/********************************************************
  Build symbol table
 *********************************************************/
void InitializeTable( SymbolTable *table )
{
    int i;

    for(i = 0 ; i < 26; i++)
        table->table[i] = Notype;
}

/************************ My Code ************************/
/**
 * Let second parameter a string.
 * Add the identifier form the first row of table,
 * and check whether the identifier has been declared.
 */
void add_table( SymbolTable *table, char *str, DataType t )
{
    int index;
    for(index = 0;index < 26;index++){
        if(table->table[index] == Notype){
            table->table[index] = t;
            table->name[index] = (char *)malloc( strlen(str) + 1);
            strcpy(table->name[index], str);
            break;
        }
        if(strcmp(str, table->name[index]) == 0){
            printf("Error : id %s has been declared\n", str);//error
            break;
        }
    }
}
/************************ My Code ************************/

SymbolTable build( Program program )
{
    SymbolTable table;
    Declarations *decls = program.declarations;
    Declaration current;

    InitializeTable(&table);

    while(decls !=NULL){
        current = decls->first;
        add_table(&table, current.name, current.type);
        decls = decls->rest;
    }

    return table;
}


/********************************************************************
  Type checking
 *********************************************************************/

void convertType( Expression * old, DataType type )
{
    if(old->type == Float && type == Int){
        printf("error : can't convert float to integer\n");
        return;
    }
    if(old->type == Int && type == Float){
        Expression *tmp = (Expression *)malloc( sizeof(Expression) );
        if(old->v.type == Identifier)
            printf("convert to float %s \n",old->v.val.id); /** change from %c to %s */
        else
            printf("convert to float %d \n", old->v.val.ivalue);
        tmp->v = old->v;
        tmp->leftOperand = old->leftOperand;
        tmp->rightOperand = old->rightOperand;
        tmp->type = old->type;

        Value v;
        v.type = IntToFloatConvertNode;
        v.val.op = IntToFloatConvert;
        old->v = v;
        old->type = Int;
        old->leftOperand = tmp;
        old->rightOperand = NULL;
    }
}

DataType generalize( Expression *left, Expression *right )
{
    if(left->type == Float || right->type == Float){
        printf("generalize : float\n");
        return Float;
    }
    printf("generalize : int\n");
    return Int;
}

/************************ My Code ************************/
/*
 * Use sequential search to check whether the identifier is in the symbol table and return the type.
 */
DataType lookup_table( SymbolTable *table, char *str )
{
    int index;
    for(index = 0;index < 26;index++){
        if(table->table[index] != Int && table->table[index] != Float){
            printf("Error : identifier %s is not declared\n", str);//error
            break;
        }
        if(strcmp(table->name[index], str) == 0){
            break;
        }
    }

    return table->table[index];
}
/************************ My Code ************************/

void checkexpression( Expression * expr, SymbolTable * table )
{
    char c;
    if(expr->leftOperand == NULL && expr->rightOperand == NULL){
        switch(expr->v.type){
            case Identifier:
                printf("identifier : %s\n", expr->v.val.id);         /** %c to %s */
                expr->type = lookup_table(table, expr->v.val.id);    /** pass string rather than char. */
                break;
            case IntConst:
                printf("constant : int\n");
                expr->type = Int;
                break;
            case FloatConst:
                printf("constant : float\n");
                expr->type = Float;
                break;
                //case PlusNode: case MinusNode: case MulNode: case DivNode:
            default:
                break;
        }
    }
    else{
        Expression *left = expr->leftOperand;
        Expression *right = expr->rightOperand;

        checkexpression(left, table);
        checkexpression(right, table);

        DataType type = generalize(left, right);
        expr->type = type; /** Change order, assign type earlier */

        /************************ My Code ************************/
        /**
         * constant folding
         */
        if( (left->v.type == IntConst || left->v.type == FloatConst) && (right->v.type == IntConst || right->v.type == FloatConst) ){
            float operand1, operand2, tmp;

            if(left->v.type == IntConst)
                operand1 = left->v.val.ivalue;
            else
                operand1 = left->v.val.fvalue;

            if(right->v.type == IntConst)
                operand2 = right->v.val.ivalue;
            else
                operand2 = right->v.val.fvalue; /** get two operand */

            switch(expr->v.type){
                case PlusNode:
                    tmp = operand1 + operand2;
                    break;
                case MinusNode:
                    tmp = operand1 - operand2;
                    break;
                case MulNode:
                    tmp = operand1 * operand2;
                    break;
                case DivNode:
                    tmp = operand1 / operand2;
                    break;
            } /** decide correct operation */

            if(expr->type == Int){
                expr->v.val.ivalue = (int)tmp;
                expr->v.type = IntConst;
            }
            else if(expr->type = Float){
                expr->v.val.fvalue = tmp;
                expr->v.type = FloatConst;
            } /** change the operation node to constant node with correct value. */

            expr->leftOperand = NULL;
            expr->rightOperand = NULL;

            return;
        }
        /************************ My Code ************************/

        convertType(left, type);//left->type = type;//converto
        convertType(right, type);//right->type = type;//converto
    }
}

void checkstmt( Statement *stmt, SymbolTable * table )
{
    if(stmt->type == Assignment){
        AssignmentStatement assign = stmt->stmt.assign;
        printf("assignment : %s \n", assign.id); /** change from %c to %s */
        checkexpression(assign.expr, table);
        stmt->stmt.assign.type = lookup_table(table, assign.id);
        if (assign.expr->type == Float && stmt->stmt.assign.type == Int) {
            printf("error : can't convert float to integer\n");
        } else {
            convertType(assign.expr, stmt->stmt.assign.type);
        }
    }
    else if (stmt->type == Print){
        printf("print : %s \n",stmt->stmt.variable); /** Change from %c to %s. */
        lookup_table(table, stmt->stmt.variable);
    }
    else printf("error : statement error\n");//error
}

void check( Program *program, SymbolTable * table )
{
    Statements *stmts = program->statements;
    while(stmts != NULL){
        checkstmt(&stmts->first,table);
        stmts = stmts->rest;
    }
}


/***********************************************************************
  Code generation
 ************************************************************************/
void fprint_op( FILE *target, ValueType op )
{
    switch(op){
        case MinusNode:
            fprintf(target,"-\n");
            break;
        case PlusNode:
            fprintf(target,"+\n");
            break;
        /************************ My Code ************************/
        /**
         * Add the case to generate operator of multiply and divide.
         */
        case MulNode:
            fprintf(target,"*\n");
            break;
        case DivNode:
            fprintf(target,"/\n");
            break;
        /************************ My Code ************************/
        default:
            fprintf(target,"Error in fprintf_op ValueType = %d\n",op);
            break;
    }
}

/************************ My Code ************************/
/**
 * Use sequential search to find the identifier and map it to a char.
 * Mapping function is 1st identifier -> a
 *                     2nd identifier -> b
 *                     ... and so on.
 */
char find_register(char *str, SymbolTable *table) {
    int index;
    for(index = 0;index < 26;index++){
        if(table->table[index] == Notype)
            break;
        if(strcmp(str, table->name[index]) == 0)
            return index + 'a';
    }

    fprintf(stderr, "Should not be here.\n");
    return '!';
}
/************************ My Code ************************/

void fprint_expr( FILE *target, Expression *expr, SymbolTable *table) /** Add table as parameter */
{

    if(expr->leftOperand == NULL){
        switch( (expr->v).type ){
            case Identifier:
                fprintf(target,"l%c\n", find_register( (expr->v).val.id, table)); /** Convert identifier to register character. */
                break;
            case IntConst:
                fprintf(target,"%d\n",(expr->v).val.ivalue);
                break;
            case FloatConst:
                fprintf(target,"%f\n", (expr->v).val.fvalue);
                break;
            default:
                fprintf(target,"Error In fprint_left_expr. (expr->v).type=%d\n",(expr->v).type);
                break;
        }
    }
    else{
        fprint_expr(target, expr->leftOperand, table); /** Add table as parameter */
        if(expr->rightOperand == NULL){ // IntToFloatConvertNode
            fprintf(target,"5k\n");
        }
        else{
            //	fprint_right_expr(expr->rightOperand);
            fprint_expr(target, expr->rightOperand, table); /** Add table as parameter */
            /************************ My Code ************************/
            /**
             * Fix TA's bug of two Float's operation.
             */
            if(expr->leftOperand->type == Float && expr->rightOperand->type == Float){
                fprintf(target,"5k\n");
            }
            /************************ My Code ************************/
            fprint_op(target, (expr->v).type);
            /************************ My Code ************************/
            /**
             * Fix TA's bug of two Float's operation.
             */
            if(expr->leftOperand->type == Float && expr->rightOperand->type == Float){
                fprintf(target,"0k\n");
            }
            /************************ My Code ************************/
        }
    }
}

void gencode(Program prog, FILE * target, SymbolTable *table) /** Add table as parameter */
{
    Statements *stmts = prog.statements;
    Statement stmt;

    while(stmts != NULL){
        stmt = stmts->first;
        switch(stmt.type){
            case Print:
                fprintf(target,"l%c\n", find_register(stmt.stmt.variable, table)); /** Convert identifier to register character. */
                fprintf(target,"p\n");
                break;
            case Assignment:
                fprint_expr(target, stmt.stmt.assign.expr, table); /** Add table as parameter */
                /*
                   if(stmt.stmt.assign.type == Int){
                   fprintf(target,"0 k\n");
                   }
                   else if(stmt.stmt.assign.type == Float){
                   fprintf(target,"5 k\n");
                   }*/
                fprintf(target,"s%c\n", find_register(stmt.stmt.assign.id, table)); /** Convert identifier to register character. */
                fprintf(target,"0 k\n");
                break;
        }
        stmts=stmts->rest;
    }

}


/***************************************
  For our debug,
  you can omit them.
 ****************************************/
//void print_expr(Expression *expr)
//{
    //if(expr == NULL)
        //return;
    //else{
        //print_expr(expr->leftOperand);
        //switch((expr->v).type){
            //case Identifier:
                //printf("%c ", (expr->v).val.id);
                //break;
            //case IntConst:
                //printf("%d ", (expr->v).val.ivalue);
                //break;
            //case FloatConst:
                //printf("%f ", (expr->v).val.fvalue);
                //break;
            //case PlusNode:
                //printf("+ ");
                //break;
            //case MinusNode:
                //printf("- ");
                //break;
            //case MulNode:
                //printf("* ");
                //break;
            //case DivNode:
                //printf("/ ");
                //break;
            //case IntToFloatConvertNode:
                //printf("(float) ");
                //break;
            //default:
                //printf("error ");
                //break;
        //}
        //print_expr(expr->rightOperand);
    //}
//}

//void test_parser( FILE *source )
//{
    //Declarations *decls;
    //Statements *stmts;
    //Declaration decl;
    //Statement stmt;
    //Program program = parser(source);

    //decls = program.declarations;

    //while(decls != NULL){
        //decl = decls->first;
        //if(decl.type == Int)
            //printf("i ");
        //if(decl.type == Float)
            //printf("f ");
        //[>*********************** My Code ***********************<]
        /**
         * Print string rather than char.
         */
        //printf("%s ",decl.name);
        //[>*********************** My Code ***********************<]
        //decls = decls->rest;
    //}

    //stmts = program.statements;

    //while(stmts != NULL){
        //stmt = stmts->first;
        //if(stmt.type == Print){
            //printf("p %c ", stmt.stmt.variable);
        //}

        //if(stmt.type == Assignment){
            //printf("%c = ", stmt.stmt.assign.id);
            //print_expr(stmt.stmt.assign.expr);
        //}
        //stmts = stmts->rest;
    //}

//}
